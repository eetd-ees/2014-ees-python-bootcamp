# -*- coding: utf-8 -*-
"""
Created on Wed Oct 22 21:16:53 2014

@author: mganesh
"""

import numpy as np

pi = 3.141592653589793
e = np.exp(1)

def adder(x,y,greeting="Welcome. I'll be your adder for today"):
    '''
    Friendly function to add two numbers togeter
    '''
    print greeting
    z = x + y
    return z

def restaurant_bill_w_tip(bill,tip = 0.15):
    '''
    Figure out the restaurant bill including tip
    '''
    return bill * (1 + tip)

def is_palindrome(word):
    '''
    Is the input word a palindrome or not
    '''
    return word == word[::-1]
    
def is_even(num):
    '''
    Is th inpute number even. Return True if it is.
    '''
    if num % 2 == 0:
        out = True
    else:
        out = False
    return out
    