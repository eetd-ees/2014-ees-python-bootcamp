{
 "metadata": {
  "name": "",
  "signature": "sha256:4af6be5a100895edc5ea6069dd6433faf47f6b65fcaa676dffcfa02ae7748bc5"
 },
 "nbformat": 3,
 "nbformat_minor": 0,
 "worksheets": [
  {
   "cells": [
    {
     "cell_type": "heading",
     "level": 1,
     "metadata": {},
     "source": [
      "An Introduction to Numerical Computing with Python"
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "While the Python language is an excellent tool for general-purpose programming, with a highly readable syntax, rich and powerful data types (strings, lists, sets, dictionaries, arbitrary length integers, etc) and a very comprehensive standard library, it was not designed specifically for mathematical and scientific computing.  Neither the language nor its standard library have facilities for the efficient representation of multidimensional datasets, tools for linear algebra and general matrix manipulations (an essential building block of virtually all technical computing), nor any data visualization facilities.\n",
      "\n",
      "In particular, Python lists are very flexible containers that can be nested arbitrarily deep and which can hold any Python object in them, but they are poorly suited to represent efficiently common mathematical constructs like vectors and matrices.  In contrast, much of our modern heritage of scientific computing has been built on top of libraries written in the Fortran language, which has native support for vectors and matrices as well as a library of mathematical functions that can efficiently operate on entire arrays at once."
     ]
    },
    {
     "cell_type": "heading",
     "level": 2,
     "metadata": {},
     "source": [
      "Basics of Numpy arrays"
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "We now turn our attention to the Numpy library, which forms the base layer for the entire 'scipy ecosystem'.  Once you have installed numpy, you can import it as"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": true,
     "input": [
      "import numpy as np"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "The main object provided by numpy is a powerful array.  We'll start by exploring how the numpy array differs from Python lists.  We start by creating a simple list and an array with the same contents of the list:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": true,
     "input": [
      "lst = [10, 20, 30, 40]\n",
      "arr = np.array([10, 20, 30, 40])"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Elements of a one-dimensional array are accessed with the same syntax as a list:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "lst[0]"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "arr[0]"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "arr[-1]"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "arr[2:]"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "The first difference to note between lists and arrays is that arrays are *homogeneous*; i.e. all elements of an array must be of the same type.  In contrast, lists can contain elements of arbitrary type. For example, we can change the last element in our list above to be a string:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "lst[-1] = 'a string inside a list'\n",
      "lst"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "but the same can not be done with an array, as we get an error message:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "arr[-1] = 'a string inside an array'"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "The information about the type of an array is contained in its *dtype* attribute:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "arr.dtype"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Once an array has been created, its dtype is fixed and it can only store elements of the same type.  For this example where the dtype is integer, if we store a floating point number it will be automatically converted into an integer:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "arr[-1] = 1.234\n",
      "arr"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Above we created an array from an existing list; now let us now see other ways in which we can create arrays, which we'll illustrate next.  A common need is to have an array initialized with a constant value, and very often this value is 0 or 1 (suitable as starting value for additive and multiplicative loops respectively); `zeros` creates arrays of all zeros, with any desired dtype:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "np.zeros(5, float)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "np.zeros(3, int)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "np.zeros(3, complex)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "and similarly for `ones`:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "print '5 ones:', np.ones(5)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "If we want an array initialized with an arbitrary value, we can create an empty array and then use the fill method to put the value we want into the array:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "a = np.empty(4)\n",
      "a.fill(5.5)\n",
      "a"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Numpy also offers the `arange` function, which works like the builtin `range` but returns an array instead of a list:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "np.arange(5)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "and the `linspace` and `logspace` functions to create linearly and logarithmically-spaced grids respectively, with a fixed number of points and including both ends of the specified interval:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "print \"A linear grid between 0 and 1:\", np.linspace(0, 1, 5)\n",
      "print \"A logarithmic grid between 10**1 and 10**4: \", np.logspace(1, 4, 4)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Finally, it is often useful to create arrays with random numbers that follow a specific distribution.  The `np.random` module contains a number of functions that can be used to this effect, for example this will produce an array of 5 random samples taken from a standard normal distribution (0 mean and variance 1):"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "np.random.randn(5)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "whereas this will also give 5 samples, but from a normal distribution with a mean of 10 and a variance of 3:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "norm10 = np.random.normal(10, 3, 5)\n",
      "norm10"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "heading",
     "level": 2,
     "metadata": {},
     "source": [
      "Indexing with other arrays"
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Above we saw how to index arrays with single numbers and slices, just like Python lists.  But arrays allow for a more sophisticated kind of indexing which is very powerful: you can index an array with another array, and in particular with an array of boolean values.  This is particluarly useful to extract information from an array that matches a certain condition.\n",
      "\n",
      "Consider for example that in the array `norm10` we want to replace all values above 9 with the value 0.  We can do so by first finding the *mask* that indicates where this condition is true or false:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "mask = norm10 > 9\n",
      "mask"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Now that we have this mask, we can use it to either read those values or to reset them to 0:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "print 'Values above 9:', norm10[mask]"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "print 'Resetting all values above 9 to 0...'\n",
      "norm10[mask] = 0\n",
      "print norm10"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "heading",
     "level": 2,
     "metadata": {},
     "source": [
      "Arrays with more than one dimension"
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Up until now all our examples have used one-dimensional arrays.  But Numpy can create arrays of aribtrary dimensions, and all the methods illustrated in the previous section work with more than one dimension.  For example, a list of lists can be used to initialize a two dimensional array:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "lst2 = [[1, 2], [3, 4]]\n",
      "arr2 = np.array([[1, 2], [3, 4]])\n",
      "arr2"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "With two-dimensional arrays we start seeing the power of numpy: while a nested list can be indexed using repeatedly the `[ ]` operator, multidimensional arrays support a much more natural indexing syntax with a single `[ ]` and a set of indices separated by commas:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "print lst2[0][1]\n",
      "print arr2[0,1]"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Most of the array creation functions listed above can be used with more than one dimension, for example:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "np.zeros((2,3))"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "np.random.normal(10, 3, (2, 4))"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "In fact, the shape of an array can be changed at any time, as long as the total number of elements is unchanged.  For example, if we want a 2x4 array with numbers increasing from 0, the easiest way to create it is:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "arr = np.arange(8).reshape(2,4)\n",
      "print arr"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "With multidimensional arrays, you can also use slices, and you can mix and match slices and single indices in the different dimensions (using the same array as above):"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "print 'Slicing in the second row:', arr[1, 2:4]\n",
      "print 'All rows, third column   :', arr[:, 2]"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "If you only provide one index, then you will get an array with one less dimension containing that row:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "print 'First row:  ', arr[0]\n",
      "print 'Second row: ', arr[1]"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Now that we have seen how to create arrays with more than one dimension, it's a good idea to look at some of the most useful properties and methods that arrays have.  The following provide basic information about the size, shape and data in the array:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "print 'Data type                :', arr.dtype\n",
      "print 'Total number of elements :', arr.size\n",
      "print 'Number of dimensions     :', arr.ndim\n",
      "print 'Shape (dimensionality)   :', arr.shape\n",
      "print 'Memory used (in bytes)   :', arr.nbytes"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Arrays also have many useful methods, some especially useful ones are:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "print 'Minimum and maximum             :', arr.min(), arr.max()\n",
      "print 'Sum and product of all elements :', arr.sum(), arr.prod()\n",
      "print 'Mean and standard deviation     :', arr.mean(), arr.std()"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "For these methods, the above operations area all computed on all the elements of the array.  But for a multidimensional array, it's possible to do the computation along a single dimension, by passing the `axis` parameter; for example:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "print 'For the following array:\\n', arr\n",
      "print 'The sum of elements along the rows is    :', arr.sum(axis=1)\n",
      "print 'The sum of elements along the columns is :', arr.sum(axis=0)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "As you can see in this example, the value of the `axis` parameter is the dimension which will be *consumed* once the operation has been carried out.  This is why to sum along the rows we use `axis=0`.  \n",
      "\n",
      "This can be easily illustrated with an example that has more dimensions; we create an array with 4 dimensions and shape `(3,4,5,6)` and sum along the axis number 2 (i.e. the *third* axis, since in Python all counts are 0-based).  That consumes the dimension whose length was 5, leaving us with a new array that has shape `(3,4,6)`:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "np.zeros((3,4,5,6)).sum(2).shape"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Another widely used property of arrays is the `.T` attribute, which allows you to access the transpose of the array:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "print 'Array:\\n', arr\n",
      "print 'Transpose:\\n', arr.T"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "We don't have time here to look at all the methods and properties of arrays, here's a complete list.  Simply try exploring some of these IPython to learn more, or read their description in the full Numpy documentation:\n",
      "\n",
      "    arr.T             arr.copy          arr.getfield      arr.put           arr.squeeze\n",
      "    arr.all           arr.ctypes        arr.imag          arr.ravel         arr.std\n",
      "    arr.any           arr.cumprod       arr.item          arr.real          arr.strides\n",
      "    arr.argmax        arr.cumsum        arr.itemset       arr.repeat        arr.sum\n",
      "    arr.argmin        arr.data          arr.itemsize      arr.reshape       arr.swapaxes\n",
      "    arr.argsort       arr.diagonal      arr.max           arr.resize        arr.take\n",
      "    arr.astype        arr.dot           arr.mean          arr.round         arr.tofile\n",
      "    arr.base          arr.dtype         arr.min           arr.searchsorted  arr.tolist\n",
      "    arr.byteswap      arr.dump          arr.nbytes        arr.setasflat     arr.tostring\n",
      "    arr.choose        arr.dumps         arr.ndim          arr.setfield      arr.trace\n",
      "    arr.clip          arr.fill          arr.newbyteorder  arr.setflags      arr.transpose\n",
      "    arr.compress      arr.flags         arr.nonzero       arr.shape         arr.var\n",
      "    arr.conj          arr.flat          arr.prod          arr.size          arr.view\n",
      "    arr.conjugate     arr.flatten       arr.ptp           arr.sort          "
     ]
    },
    {
     "cell_type": "heading",
     "level": 2,
     "metadata": {},
     "source": [
      "Operating with arrays"
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Arrays support all regular arithmetic operators, and the numpy library also contains a complete collection of basic mathematical functions that operate on arrays.  It is important to remember that in general, all operations with arrays are applied *element-wise*, i.e., are applied to all the elements of the array at the same time.  Consider for example:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "arr1 = np.arange(4)\n",
      "arr2 = np.arange(10, 14)\n",
      "print arr1, '+', arr2, '=', arr1+arr2"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Importantly, you must remember that even the multiplication operator is by default applied element-wise, it is *not* the matrix multiplication from linear algebra (as is the case in Matlab, for example):"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "print arr1, '*', arr2, '=', arr1*arr2"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "While this means that in principle arrays must always match in their dimensionality in order for an operation to be valid, numpy will *broadcast* dimensions when possible.  For example, suppose that you want to add the number 1.5 to `arr1`; the following would be a valid way to do it:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "arr1 + 1.5*np.ones(4)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "But thanks to numpy's broadcasting rules, the following is equally valid:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "arr1 + 1.5"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "heading",
     "level": 2,
     "metadata": {},
     "source": [
      "Linear algebra in numpy"
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Numpy ships with a basic linear algebra library, and all arrays have a `dot` method whose behavior is that of the scalar dot product when its arguments are vectors (one-dimensional arrays) and the traditional matrix multiplication when one or both of its arguments are two-dimensional arrays:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "v1 = np.array([2, 3, 4])\n",
      "v2 = np.array([1, 0, 1])\n",
      "print v1, '.', v2, '=', v1.dot(v2)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Here is a regular matrix-vector multiplication, note that the array `v1` should be viewed as a *column* vector in traditional linear algebra notation; numpy makes no distinction between row and column vectors and simply verifies that the dimensions match the required rules of matrix multiplication, in this case we have a $2 \\times 3$ matrix multiplied by a 3-vector, which produces a 2-vector:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "A = np.arange(6).reshape(2, 3)\n",
      "print A, 'x', v1, '=', A.dot(v1)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "For matrix-matrix multiplication, the same dimension-matching rules must be satisfied, e.g. consider the difference between $A \\times A^T$:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "print A.dot(A.T)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "and $A^T \\times A$:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "print A.T.dot(A)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": []
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Furthermore, the `numpy.linalg` module includes additional functionality such as determinants, matrix norms, Cholesky, eigenvalue and singular value decompositions, etc.  For even more linear algebra tools, `scipy.linalg` contains the majority of the tools in the classic LAPACK libraries as well as functions to operate on sparse matrices.  We refer the reader to the Numpy and Scipy documentations for additional details on these."
     ]
    }
   ],
   "metadata": {}
  }
 ]
}