# README #

### What is this repository for? ###

* This repository contains materials for the EES python bootcamp starting in October 2014

### How do I use it? ###

* This repository is a set of folders and files.  
* You can clone this to your computer so that you have all the same files on your computer.
* You can also find this folder on the X drive: "X:\Python bootcamp 2014"

### Who do I talk to for more information? ###

* This repo is managed by Scott Young and Mohan Ganeshalingam